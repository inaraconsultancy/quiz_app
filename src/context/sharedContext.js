import { useState, useContext, createContext } from "react";
import { Questions } from "../QuestionBank/QuestionBank";
const initialState = {
  id: 1,
  questions: [],
  score: 0,
};

const SharedContext = createContext(initialState);
export function SharedContextProvider({ children }) {
  const [score, setScore] = useState(0);
  const [id, setId] = useState(1);
  const [currentQuestion, setCurrentQuestion] = useState(null);
 
  const [submit, setSubmit] = useState(false);

  const handleTestScore = (e, answer) => {
    setCurrentQuestion(e);
    if (e.answer === answer) {
      setScore(score + 1);
    }
  };

  const value = {
    id,
    setId,
    submit,
    setSubmit,
   
    score,
    setScore,
    currentQuestion,
  };

  return (
    <SharedContext.Provider value={value}>{children}</SharedContext.Provider>
  );
}

export const useShadeContext = () => useContext(SharedContext);
export default SharedContext;
