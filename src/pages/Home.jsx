import React from 'react'
import { Link } from 'react-router-dom'

const Home = () => {




  return (
    <div className="bg-slate-400 h-[400px] rounded-3xl ">
      <Link
        to="/quiz"
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
      >
        Start Quiz
      </Link>

      <h1 className='mt-36 font-sans text-3xl'>Quiz App</h1>
    </div>
  );
}

export default Home