import React from 'react'

const Navbar = () => {
  return (
    <div className="h-[50px] flex-row bg-black rounded-3xl  text-white align-middle">
      <p className="pt-3">Welcome to quiz app</p>
    </div>
  );
}

export default Navbar