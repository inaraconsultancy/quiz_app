import React, { useEffect, useState } from "react";
import { useShadeContext } from "../context/sharedContext";
import { Questions } from "../QuestionBank/QuestionBank";
const Question = () => {
  const { id, setId, score, submit, setSubmit, setScore } = useShadeContext();
  const [answer, setAnswer] = useState([]);
  const [change, setChange] = useState("");
  const [checked, setChecked] = useState("");
  const question = Questions.find((ques) => ques.id === id);

  const handleSubmit = () => {
    setSubmit(true);
  };
  const handlereload = () => {
    window.location.reload();
  };
  const handleTestScore = (e) => {
    answer[id-1]=checked;

    setAnswer(answer);
  
    if (Questions[id - 1].answer === checked) {
      setScore(score + 1);
    }
    console.log(answer);
    console.log(id);
    setChange("");
    setChecked("");
  };

  return (
    <>
      {!submit && (
        <div className="">
          <h1 className="h-8 w-[200px] flex mx-auto justify-center font-extrabold mt-8 text-white bg-blue-500 rounded-full">
            Question No. {id}
          </h1>

          <h2 className="mt-5 bg-green-500 rounded-full h-10 pt-2 font-bold font-mono justify-center">
            {question.question}
          </h2>
          <div className="flex justify-between px-12 mt-6 bg-pink-700 h-10 pt-1 rounded-xl">
            <div>
              <input
                type="radio"
                id="option1"
                name="option"
                value={question.option1}
                className=""
                checked={
                  answer[id -1]
                    ? answer[id -1] === "option1"
                    : checked === "option1"
                }
                onChange={(e) => {
                  // handleTestScore(question, question.option1);
                  setChange(e.target.value);
                  setChecked("option1");
                }}
              />
              <label
                className="font-sans font-semibold text-white "
                htmlFor="option1"
              >
                {question.option1}
              </label>
            </div>
            <div>
              <input
                type="radio"
                className=""
                id="option2"
                name="option"
                value={question.option2}
                checked={
                  answer[id - 1]
                    ? answer[id - 1] === "option2"
                    : checked === "option2"
                }
                onChange={(e) => {
                  setChange(e.target.value);
                  setChecked("option2");
                }}
              />
              <label
                htmlFor="option2"
                className="font-sans font-semibold text-white "
              >
                {question.option2}
              </label>
            </div>

            <div>
              <input
                type="radio"
                className=""
                id="option3"
                name="option"
                value={question.option3}
                checked={
                  answer[id - 1]
                    ? answer[id - 1] === "option3"
                    : checked === "option3"
                }
                onChange={(e) => {
                  setChange(e.target.value);
                  setChecked("option3");
                }}
              />
              <label
                htmlFor="option3"
                className="font-sans font-semibold text-white "
              >
                {question.option3}
              </label>
            </div>
          </div>
          <div className="flex justify-between mt-10">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
              onClick={() =>
                setId((prevId) => {
                  if (prevId !== 1) return prevId - 1;
                  return 1;
                })
              }
            >
              Prev
            </button>
            {id === Questions.length ? (
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
                onClick={() => {
                  handleSubmit();
                  handleTestScore();
                }}
              >
                Submit
              </button>
            ) : (
              <button
                className="bg-blue-500  hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
                onClick={() => {
                  handleTestScore();
                  setId((prevId) => {
                    const newId = prevId + 1;
                    // nextque(newId);
                    if (newId > Questions.length - 1) {
                      return Questions.length;
                    } else {
                      return newId;
                    }
                  });
                  setChange("");
                }}
              >
                Next
              </button>
            )}{" "}
          </div>
        </div>
      )}
      {submit && (
        <div
          onClick={handlereload}
          className="w-[500px] h-[30px] mt-12 bg-black rounded-2xl"
        >
          <p className="font-mono  text-white py-1">
            {" "}
            final score is {score} , You need to click here
          </p>
        </div>
      )}
    </>
  );
};

export default Question;
