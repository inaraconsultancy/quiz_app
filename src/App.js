import './App.css';
import Question from './component/Question';
import Navbar from './component/Navbar';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from './pages/Home';

function App() {
  return (
    <div className="App w-[500px]  h-[500px] mx-auto my-14  ">
    <Router>

    <Navbar></Navbar>
    <Routes>
    <Route exact path='/' element={<Home/>} ></Route>
    <Route exact path='/quiz'element={<Question/>} />
    
    </Routes>
    </Router>
    </div>
  );
}

export default App;
